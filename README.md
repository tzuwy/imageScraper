[Updated Version](https://gitlab.com/tzuwychew/image-scraper)


---



# Image-Scraper
scrapes images from various sites and fixes paths

#### Supported Sites:
* tistory (grabs original images)
* wordpress (removes url queries)
* XpressEngine
* Lofter
* Naver (removes url queries and replaces subdomain)

### Requirements
* Python 3
* urllib
* BeautifulSoup4
* requests
* html5lib
