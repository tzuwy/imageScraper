import multiprocessing
import os
import urllib
import urllib.request
import urllib.parse
import re
import time
import requests

from bs4 import BeautifulSoup
from urllib.parse import urlparse

# create async downoad processes
def addDownload(original, folder, alt):
	p = multiprocessing.Process(target=download, args=(original, folder, alt))
	# add job
	jobs.append(p)
	p.start()

# worker download process
def download(url, folder, alt):
	if alt == "none":
		#grab file name if no alt/image name provided
		r = requests.get(url)
		d = r.headers['content-disposition']
		fname = re.findall("filename=(.+)", d)
		# get first list string
		fnamestring = fname[0]
		# remove quotes in string
		HttpFilename = fnamestring[1:-1]

		urllib.parse.unquote(HttpFilename)
		filename = urllib.parse.unquote(HttpFilename)
	else:
		filename = alt
		
	# create path and file name
	file = folder + "/" + filename
	urllib.request.urlretrieve(url, file)
	print("Downloaded " + url)
	return

if __name__ == '__main__':
	# inputs
	url = input("Enter url: ")
	folder = input("Enter folder: ")
	# create directory if it doesn't exist already
	if not os.path.exists(folder):
		os.makedirs(folder)
	# get url
	source_code = requests.get(url)
	# convert to plain text
	plain_text = source_code.text
	soup = BeautifulSoup(plain_text, "html5lib")
	# multiprocessing jobs
	jobs = []
	# init count of images
	count = 0
	throttle = 0
	print("\n[Info] Grabbing links...")

	for image in soup.find_all('img'):
		# get image links
		src = image.get('src')
	
		# TISTORY SITE - IMAGE LINKS
		if ".com/image" in src:
			# print message only once
			if count == 0:
				print("[Info] tistory.com site detected, grabbing original urls")
			# create original image path
			original = src.replace("image", "original")
			# print original url
			print(original)
			# increment count by 1
			count += 1
			# start async shit
			addDownload(original, folder, "none")
		
		# TISTORY SITE - ORIGINAL LINKS
		elif ".com/original" in src:
			# print message only once
			if count == 0:
				print("[Info] tistory.com original images detected")
			# print original url
			print(src)
			# increment count by 1
			count += 1
			# start async shit
			addDownload(src, folder, "none")

		# WORDPRESS SITE (ex: flawlessapril)
		elif "wordpress.com" in src:
			# print message only once
			if count == 0:
				print("[Info] wordpress site detected, removing url parameters - checking for ?w=960")
			alt = image.get('alt') + ".jpg"
			# remove url queries
			srcParse = urlparse(src)
			original = srcParse.scheme + "://" + srcParse.netloc + srcParse.path
			# print original url
			print(original)
			# increment count by 1
			count += 1
			# start async shit
			addDownload(original, folder, alt)
			
		# XPRESSENGINE (ex: twicestudio1020)
		elif "/xe/files/attach" in src:
			# print message only once
			if count == 0:
				print("[Info] XpressEngine site detected, grabbing image alt names")
			alt = image.get('alt')
			# print url
			print(src)
			# increment count by 1
			count += 1
			# start async shit
			addDownload(src, folder, alt)

		#lofter (ex: samyangsy.lofter.com)
		elif "127.net/img" in src:
			# print message only once
			if count == 0:
				print("[Info] lofter site detected")
			# remove url queries
			srcParse = urlparse(src)
			original = srcParse.scheme + "://" + srcParse.netloc + srcParse.path
			# make file name from path
			pathName = original.split('/')[-1]
			# print url
			print(original)
			# increment count by 1
			count += 1
			# start async shit
			addDownload(original, folder, pathName)
		elif "naver.net/2016" in src:
			if count == 0:
				print("[Info] naver site detected")
			# remove url queries
			srcParse = urlparse(src)
			subdomain = srcParse.hostname.split('.')[0]
			originalNoQuery = srcParse.scheme + "://" + srcParse.netloc + srcParse.path
			original = originalNoQuery.replace(subdomain, "blogfiles")
			# make file name from path
			pathName = original.split('/')[-1]
			# print url
			print(original)
			# increment count by 1
			count += 1
			# start async shit
			addDownload(original, folder, pathName)
			
		# delay each loop by 1s to not dos the sites, shitty rate throttler
		time.sleep(1)
	# start download...
	print("\n\n=====================================================")
	print("[Info] Finished scan... saving " + str(count) + " images to " + folder + "...\n\n")